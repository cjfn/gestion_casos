<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('empresas','empresasController', ['only'=>['index','store','show','update','destroy']]);
Route::resource('estados','estadosController', ['only'=>['index','store','show','update','destroy']]);
Route::resource('usuario','usuarioController', ['only'=>['index','store','show','update','destroy']]);
Route::resource('tipos_usuarios','tipo_usuarioController', ['only'=>['index','store','show','update','destroy']]);
Route::resource('tipo_casos','tipo_casoController', ['only'=>['index','store','show','update','destroy']]);
Route::resource('paises','paisesController', ['only'=>['index','store','show','update','destroy']]);

Route::resource('oficinas','oficinasController', ['only'=>['index','store','show','update','destroy']]);
Route::resource('gestion_casos','gestion_casosController', ['only'=>['index','store','show','update','destroy']]);
Route::get('getOficinas/{empresa}','oficinasController@getOficinas')->name('getOficinas');
Route::get('getEstados/{empresa}','estadosController@getEstados')->name('getEstados');
Route::get('getTipoCasos/{empresa}','tipo_casoController@getTipoCasos')->name('getTipoCasos');
Route::get('getUsuarios/{empresa}','usuarioController@getUsuarios')->name('getUsuarios');


//Route::put('empresa/{id}','municipioController@update')->name('update');


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
