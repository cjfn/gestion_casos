<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' => 'auth'], function () {

Route::get('/home', 'HomeController@index');
Route::get('empresas', 'web\empresasController@index');
Route::get('estados', 'web\estadosController@index');
Route::get('usuario', 'web\usuariosnController@index');
Route::get('tipo_casos', 'web\tipo_casoController@index');
Route::get('oficinas', 'web\oficinasController@index');
Route::get('gestion_casos', 'web\gestion_casosController@index');
Route::post('gestion_casos/save', 'web\gestion_casosController@save');

});
