<?php

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class gestion_casosController extends Controller
{
    //
    public function index()
    {
            return view('gestion_casos.index');
    }

    public function save(Request $request)
	{
	 
		   if ($request->hasFile('file')) {
		        $image = $request->file('file');
		        $name = time().'.'.$image->getClientOriginalExtension();
		        $destinationPath = public_path('/storage/');
		        $image->move($destinationPath, $name);
		        $image->save();
		        return back()->with('success','File Upload successfully');
		      }
		   else
		   {
				return 'No file selected';
		   }
	}

	
}
