<?php

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class empresasController extends Controller
{
    //
  public function index()
    {
            return view('empresas.index');
    }
    
   public function add(Request $request)
        {

          /*
           'name', 'email', 'password','nit','telefono','departamento','municipio','direccion', 'observaciones','tipo_usuario','tipo_entrega','activo', 'created_at', 'updated_at'
          */
            $data = new User;
            $data -> name = $request -> name;
            $nombre = explode((" "), $data -> name);
            $nombre1 = mb_strtolower($nombre[0]);
            $nombre2 = mb_strtolower($nombre[1]);
            $cod = random_int(10, 99);
            $data -> email = $nombre1.$nombre2.$cod;
            $data -> password = bcrypt(/*$request -> password*/'user123');
            $data -> telefono = $request -> telefono;
            $data -> id_departamento = $request -> departamento;
            $data -> id_municipio = $request -> municipio;
            $data -> direccion = $request -> direccion;
            $data -> observaciones = $request -> observaciones;
            $data -> tipo_usuario = $request -> tipo_usuario;
            $data -> tipo_entrega = $request -> tipo_entrega;
            $data -> descuento = $request -> descuento;
            $data -> save();
            return back()
                    ->with('success','Nombre de usuario: '.$data->email.' contraseña user123');

        }

        /*
         * View data
         */
        public function view(Request $request)
        {
            if($request->ajax()){
                $id = $request->id;
                //$info = User::find($id);
                
                //alert($id)
                $info = DB::table("users as u")
          ->join ("departamento as d","u.id_departamento","=","d.id")
          ->join ("municipio as m","u.id_municipio","=","m.id")
          ->select("u.id","u.name","u.email","u.nit","u.observaciones","u.telefono","u.tipo_entrega","u.tipo_usuario","u.updated_at","u.id_departamento","u.id_municipio","d.nombre_departamento","m.nombre_municipio","u.direccion","u.descuento", "u.created_at", "u.updated_at")
          
          ->orderBy('u.name','ASC')
          ->where('u.id','=',$id)
          ->first();
              
                //echo json_decode($info);
                return response()->json($info);
            }
        }

      

         /*
        *   Update data
        */
        public function update(Request $request)
        {
            $id = $request -> edit_id;
            $data = User::find($id);
            $data -> name = $request -> edit_nombre;
            $data -> password = bcrypt("user123");
            $data -> telefono = $request -> edit_telefono;
            $data -> id_departamento = $request -> edit_departamento;
            $data -> id_municipio = $request -> edit_municipio;
            $data -> direccion = $request -> edit_direccion;
            $data -> observaciones = $request -> edit_observaciones;
            $data -> tipo_usuario = $request -> edit_tipo_usuario;
            $data -> tipo_entrega = $request -> edit_tipo_entrega;
            $data -> descuento = $request -> edit_descuento;
            $data -> created_at = Carbon::now();;
            $data -> save();
            return back()
                    ->with('finalizado','Datos modificados exitosamente.');
        }
                /*
        *   Update data mi usuario
        */
        public function myupdate(Request $request)
        {
            $id = Auth::user()->id;
            $data = user::find($id);
            $data -> password = bcrypt($request -> edit_pass);
            $data -> save();
            return back()
                    ->with('finalizado','Datos modificados exitosamente.');
        }
 
 
        /*
        *   Delete record
        */
        public function delete(Request $request)
        {
           $id = $request -> id;
            $data = user::find($id);
             $data -> activo = false;
            //$response = $data -> update();
             $response = $data -> delete();
            if($response)
                echo "Dato eliminado exitosamente.";
            else
                echo "There was a problem. Please try again later.";
        }

}
