<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\empresas;
use App\User;
use DB;

class empresasController extends Controller
{
    //
    public function index()
    {
        //
         //$data = empresas::all()->toArray();

    		$data = DB::select("CALL sp_select_empresas()"); 

            return response()->json($data);

    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try{
           $data = DB::select("CALL sp_insert_empresas(?,?,?)",array($request->nombre,$request->direccion,$request->web)); 
            return response()->json(['status'=>true, 'respuesta'=>'Dato agregado exitosamente!'],200);
        }
        catch(Exception $e)
        {

            return response()->json(['status'=>false, 'error'],400);
        }

    }

     public function store_detalle(Request $request)
    {
        //

       
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
          try{
      
       		//$info=detallespedidos::find($id)->toArray();
            $data = DB::select("CALL sp_select_empresas_byId($id)",[$id]); 
            return response()->json($data);
        }
        catch(Exception $e)
        {
            return response()->json(['status'=>false, 'error'],400);    
        }

                
            
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   

    public function update(Request $request)
    {
    	try{
            $id = $request -> id;
            $data = empresas::find($id);
            $data -> nombre = $request -> edit_nombre;
            $data -> direccion = $request -> edit_direccion;
            $data -> web = $request -> edit_web;
            
            $data -> save();
            return back()
                    ->with('finalizado','Datos modificados exitosamente.');

            /*
           $data = DB::select("CALL sp_update_empresas(?,?,?,?)",array($id,$nombre,$direccion,$web)); 
            return response()->json(['status'=>true, 'respuesta'=>'Dato modificado exitosamente!'],200);
            */
        }
        catch(Exception $e)
        {

            return response()->json(['status'=>false, 'respuesta'=> $e],400);
        }

       
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
            try{
           $data = DB::select("CALL sp_delete_empresas(?)",array($id)); 
            return response()->json(['status'=>true, 'Dato eliminado exitosamente!'],200);
        }
        catch(Exception $e)
        {

            return response()->json(['status'=>false, 'error'],400);
        }

     

    }

}
