<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\gestion_casos;
use App\User;
use DB;
use Carbon\Carbon;

class gestion_casosController extends Controller
{
    //
    public function index()
    {
        //
         //$data = empresas::all()->toArray();

    		$data = DB::select("CALL sp_select_gestion_casos()"); 

            return response()->json($data);

    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try{

            $nombre_usuario_asignado = DB::table("users")
                ->where("id","=",$request->usuario_asignado)
                ->select("name")
              ->first();

              $data = new gestion_casos;
	            $data -> estado = $request -> estado;
	            $data -> tipo_casos = $request -> tipo_caso;
	            $data -> usuario_asignado = $request -> usuario_asignado;
	            $data -> oficina = $request -> oficina;
	            $data -> nombre_usuario_asignado = $nombre_usuario_asignado->name;
	            $data -> created_by = $request -> created_by;
	            $data -> save();
        	/*
           $data = DB::select("CALL sp_inserta_gestion_casos(?,?,?,?,?,?)",array($request->tipo_caso,$request->estado,$request->usuario_asignado,$request->oficina,$nombre_usuario_asignado->name,$request->created_by)); */
           
           return response()->json(['status'=>true, 'respuesta'=>'Dato agregado exitosamente!'],200);
           
        }
        catch(Exception $e)
        {

            return response()->json(['status'=>false, 'error'=>$e],400);
        }

    }

     public function store_detalle(Request $request)
    {
        //

       
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
          try{
      
       		//$info=detallespedidos::find($id)->toArray();
            $data = DB::select("CALL   select_by_id_gestion_casos($id)",[$id]); 
            return response()->json($data);
        }
        catch(Exception $e)
        {
            return response()->json(['status'=>false, 'error'],400);    
        }

                
            
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   

    public function update(Request $request)
    {
    	try{
           $data = DB::select("CALL  sp_update_gestion_casos(?,?,?)",array($request->estado,$request->updated_by,$request->id)); 
            return response()->json(['status'=>true, 'respuesta'=>'Dato modificado exitosamente!'],200);
            
        }
        catch(Exception $e)
        {

            return response()->json(['status'=>false, 'respuesta'=> $e],400);
        }

       
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
            try{
           $data = DB::select("CALL   sp_delete_gestion_casos(?)",array($id)); 
            return response()->json(['status'=>true, 'Dato eliminado exitosamente!'],200);
        }
        catch(Exception $e)
        {

            return response()->json(['status'=>false, 'error'],400);
        }

     

    }

}
