<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;
use Auth;
use Carbon\Carbon;

class usuarioController extends Controller
{
    //
     public function index()
   	{		
   		$data = DB::select("CALL sp_select_usuarios()"); 
            return response()->json($data);
    }

    public function getUsuarios($empresa)
    {
    	 $info = DB::table("users")
                ->where("empresa","=",$empresa)
              ->get();
                return response()->json($info);
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try{

        	$pass = bcrypt($request->password);
        	$nombre = explode((" "), $request->nombres);
            $apellido = explode((" "), $request->apellidos);
            $nombre1 = mb_strtolower($nombre[0]);
            $nombre2 = mb_strtolower($apellido[0]);
            $cod = random_int(10, 99);
            $user = $nombre1.$nombre2.$cod;

        	
           $data = DB::select("CALL sp_inserta_usuario(?,?,?,?,?,?,?,?,?)",array($user,$request->nombres,$request->apellidos,$request->direccion,$request->pais,$request->email,$pass,$request->tipo_usuario,$request->empresa)); 
           

/*
            $data = new estados;
            $data -> nombre_estado = $request -> nombre_estado;
            $data -> descripcion = $request -> descripcion;
            $data -> empresa = $request -> empresa;
            $data -> created_by = $request -> created_by;
            $data -> save();
            return response()->json(['status'=>true, 'respuesta'=>'Dato agregado exitosamente!'],200);
            */
            return response()->json(['status'=>true, 'respuesta'=>'Dato agregado exitosamente!'],200);
            
        }
        catch(Exception $e)
        {

            return response()->json(['status'=>false, 'error'=>$e],400);
        }

    }

     public function store_detalle(Request $request)
    {
        //

       
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
          try{
      
       		//$info=detallespedidos::find($id)->toArray();
            $data = DB::select("CALL sp_select_usuarios_by_id($id)",[$id]); 
            return response()->json($data);
        }
        catch(Exception $e)
        {
            return response()->json(['status'=>false, 'error'],400);    
        }

                
            
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   

    public function update(Request $request)
    {
    	try{
           $data = DB::select("CALL sp_update_estados(?,?,?,?)",array($request->nombre_estado,$request->descripcion,$request->updated_by,$request->id)); 
            return response()->json(['status'=>true, 'respuesta'=>'Dato modificado exitosamente!'],200);
            
        }
        catch(Exception $e)
        {

            return response()->json(['status'=>false, 'respuesta'=> $e],400);
        }

       
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
            try{
           $data = DB::select("CALL sp_delete_usuario(?)",array($id)); 
            return response()->json(['status'=>true, 'Dato eliminado exitosamente!'],200);
        }
        catch(Exception $e)
        {

            return response()->json(['status'=>false, 'error'],400);
        }

     

    }
}