<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class usuario extends Model
{
    //
     //
    protected $table = 'users';
	 protected $primarykey='id';
	 public $timestamps = false;
    
    protected $fillable = [
        'name',
        'nombres', 
        'apellidos', 
        'dreccion',
        'pais',
        'password',
        'tipo_usuario',
        'empresa',
        'created_at', 
        'created_by',
        'updated_at', 
        'updated_by', 
        'activo'
    ];
}
