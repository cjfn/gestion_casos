<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class tipo_usuario extends Model
{
    //
     //
     protected $table = 'tipo_usuario';
	 protected $primarykey='id';
	 public $timestamps = false;
    
    protected $fillable = [
        'nombre',
        'descripcion', 
        'empresa', 
        'created_at', 
        'created_by',
        'updated_at', 
        'updated_by', 
        'activo'
    ];
}
