<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class gestion_casos extends Model
{
    //
    protected $table = 'gestion_casos';
	 protected $primarykey='id';
	 public $timestamps = false;
    
    protected $fillable = [
        'tipo_casos', 
        'estado', 
        'usuario_asignado',
        'usuario_creador',
        'documento',
        'created_at', 
        'created_by',
        'updated_at', 
        'updated_by', 
        'activo'
    ];
}

