<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class empresas extends Model
{
    //
    //
    protected $table = 'empresas';
	 protected $primarykey='id';
	 public $timestamps = false;
    
    protected $fillable = [
        'nombre',
        'direccion', 
        'web',
        'created_at', 
        'created_by',
        'updated_at', 
        'updated_by', 
        'activo'
    ];
}
