<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class estados extends Model
{
    //
        protected $table = 'estados';
	 protected $primarykey='id';
	 public $timestamps = false;
    
    protected $fillable = [
        'nombre_estado',
        'empresa', 
        'descripcion', 
        'created_by',
        'updated_at', 
        'updated_by', 
        'activo'
    ];
}
