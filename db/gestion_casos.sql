-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-11-2019 a las 05:36:36
-- Versión del servidor: 10.1.35-MariaDB
-- Versión de PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `gestion_casos`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `select_usuario_by_id` (IN `id` BIGINT)  NO SQL
select u.*, tu.nombre as nombre_usuario, p.nombre as nombre_pais from users as u join tipo_usuario as tu on u.tipo_usuario=tu.id
join paises as p on u.pais=p.id
where u.id=id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `spEmpresas` (IN `opcion` VARCHAR(20) CHARSET utf8, IN `nombre` VARCHAR(50) CHARSET utf8, IN `direccion` VARCHAR(50) CHARSET utf8, IN `web` VARCHAR(50) CHARSET utf8, IN `id` INT)  IF opcion=1 THEN select * from empresas;
ELSEIF opcion=2 THEN INSERT INTO empresas(nombre, direccion, web) VALUES (nombre, direccion, web) ;
ELSE select * from empresas where id=id;
END IF$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_busca_by_id_tipo_usuario` (IN `id` BIGINT)  NO SQL
SELECT tu.*, e.nombre as nombre_empresa FROM tipo_usuario as tu join empresas as e on tu.empresa=e.id WHERE tu.id = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_busca_estados` ()  NO SQL
select e.*, emp.nombre as nombre_empresa from estados as e join empresas as emp on e.empresa=emp.id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_busca_estados_by_id` (IN `id` BIGINT)  NO SQL
select e.*, emp.nombre as nombre_empresa from estados as e join empresas as emp on e.empresa=emp.id where e.id=id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_busca_tipos_usuarios` ()  NO SQL
select tu.*, e.nombre as nombre_empresa from tipo_usuario as tu join empresas as e on tu.empresa=e.id order by tu.id desc$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_busca_tipo_casos` ()  NO SQL
select tc.*, e.nombre as nombre_empresa from tipo_casos as tc join empresas as e on tc.empresa=e.id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_delete_empresas` (IN `_id` INT)  NO SQL
delete FROM empresas WHERE id=_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_delete_estados` (IN `id` BIGINT)  NO SQL
DELETE FROM estados where estados.id=id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_delete_tipo_usuario` (IN `id` BIGINT)  NO SQL
DELETE from tipo_usuario WHERE 'id'=id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_empresas` (IN `_opcion` VARCHAR(20) CHARSET utf8, IN `_nombre` VARCHAR(50) CHARSET utf8, IN `_direccion` VARCHAR(50) CHARSET utf8, IN `_web` VARCHAR(50) CHARSET utf8, IN `_id` INT)  NO SQL
IF _opcion='select' THEN select * from empresas; 
	ELSEIF _opcion='selectby' THEN select * from empresas where id=_id; 
    ELSEIF _opcion='create' THEN INSERT INTO empresas(nombre, direccion, web) VALUES (_nombre, _direccion, _web); 
    ELSEIF _opcion='update' THEN UPDATE empresas SET nombre=_nombre, direccion=_direccion, web=_web, activo=_activo WHERE id=_id;
    ELSEIF _opcion='delete' THEN DELETE FROM empresas WHERE id=_id;END IF$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_estados` (IN `_opcion` VARCHAR(20) CHARSET utf8, IN `_nombre_estado` VARCHAR(50) CHARSET utf8, IN `_descripcion` VARCHAR(50) CHARSET utf8, IN `_empresa` INT, IN `_id` INT)  NO SQL
IF _opcion='select' THEN select e.*, e.nombre_estado from estados as e join empresas as em on e.empresa=em.id where activo=1; 
	ELSEIF _opcion='selectby' THEN select e.*, e.nombre_estado from estados as e join empresas as em on e.empresa=em.id where activo=1 and id=_id; 
    ELSEIF _opcion='create' THEN INSERT INTO estados(nombre_estado, descripcion, empresa ) VALUES (_nombre_estado, _descripcion, _empresa); 
    ELSEIF _opcion='update' THEN UPDATE estados SET nombre_estado=_nombre_estado, descripcion=_descripcion, empresa=_empresa, activo=_activo WHERE id=_id;
    ELSEIF _opcion='delete' THEN DELETE FROM estados WHERE id=_id;END IF$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_inserta_estados` (IN `nombre_estado` VARCHAR(50) CHARSET utf8, IN `descripcion` VARCHAR(100) CHARSET utf8, IN `empresa` BIGINT, IN `created_by` VARCHAR(50) CHARSET utf8)  NO SQL
INSERT INTO `estados` (`id`, `nombre_estado`, `descripcion`, `empresa`, `created_at`, `created_by`, `updated_at`, `updated_by`, `activo`) VALUES (NULL, nombre_estado, descripcion, empresa, CURRENT_TIMESTAMP, created_by, NULL, NULL, '1')$$

CREATE DEFINER=`localhost`@`root` PROCEDURE `sp_inserta_tipo_usuario` (IN `empresa` BIGINT, IN `nombre` VARCHAR(50) CHARSET utf8, IN `descripcion` VARCHAR(100) CHARSET utf16)  NO SQL
INSERT INTO `tipo_usuario` (`id`, `empresa`, `nombre`, `descripcion`, `created_at`, `created_by`, `updated_at`, `updated_by`, `activo`) VALUES (NULL, empresa, nombre, descripcion, CURRENT_TIMESTAMP, 'user', NULL, NULL, '1')$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_inserta_usuario` (IN `name` VARCHAR(100) CHARSET utf8, IN `nombres` VARCHAR(50) CHARSET utf8, IN `apellidos` VARCHAR(50) CHARSET utf8, IN `direccion` VARCHAR(100) CHARSET utf8, IN `pais` INT, IN `email` VARCHAR(50) CHARSET utf8, IN `password` VARCHAR(255) CHARSET utf8, IN `tipo_usuario` INT, IN `empresa` INT)  NO SQL
INSERT INTO `users` (`id`, `name`, `nombres`, `apellidos`, `direccion`, `pais`, `email`, `password`, `tipo_usuario`, `empresa`, `remember_token`, `created_at`, `updated_at`) VALUES (NULL, name, nombres, apellidos, direccion, pais, email, password, tipo_usuario, empresa, NULL, NULL, NULL)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insert_empresas` (IN `nombre` VARCHAR(50) CHARSET utf8, IN `direccion` VARCHAR(100) CHARSET utf8, IN `web` VARCHAR(50) CHARSET utf8)  NO SQL
INSERT INTO `empresas` (`id`, `nombre`, `direccion`, `web`, `created_at`, `created_by`, `updated_at`, `updated_by`, `activo`) VALUES (NULL, nombre, direccion, web, CURRENT_TIMESTAMP, '', NULL, NULL, '1')$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insert_tipo_usuario` (IN `nombre` VARCHAR(50) CHARSET utf8, IN `descripcion` VARCHAR(100) CHARSET utf8, IN `empresa` INT)  NO SQL
INSERT INTO `tipo_usuario` (`id`, `nombre`, `descripcion`, `empresa`, `created_at`, `created_by`, `updated_at`, `updated_by`, `activo`) VALUES (NULL, nombre, descripcion, empresa, CURRENT_TIMESTAMP, '', NULL, NULL, '1')$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_select_empresas` ()  NO SQL
select * from empresas$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_select_empresas_byId` (IN `_id` INT)  NO SQL
select * from empresas WHERE id=_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_select_paises` ()  NO SQL
select * from paises$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_select_tipos_usuarios` ()  NO SQL
select * from tipo_usuario$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_select_usuarios` ()  NO SQL
select u.*, tu.nombre as nombre_usuario, p.nombre as nombre_pais, e.nombre as nombre_empresa from users as u join tipo_usuario as tu on u.tipo_usuario=tu.id
join paises as p on u.pais=p.id
join empresas as e on u.empresa=e.id
ORDER by u.id ASC$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_select_usuarios_by_id` (IN `id` BIGINT)  NO SQL
select u.*, tu.nombre as nombre_usuario, p.nombre as nombre_pais, e.nombre as nombre_empresa from users as u join tipo_usuario as tu on u.tipo_usuario=tu.id
join paises as p on u.pais=p.id
join empresas as e on u.empresa=e.id
where u.id=id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_token` (IN `_pass` VARCHAR(255), IN `_email` VARCHAR(50))  NO SQL
select * from users where email=_email and password =_pass$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_update_empresas` (IN `_id` INT, IN `_nombre` VARCHAR(50), IN `_direccion` VARCHAR(100), IN `_web` VARCHAR(50))  NO SQL
UPDATE `empresas` SET `direccion` = _direccion, `nombre` = _nombre,`web` = _web, `updated_at` = NULL WHERE `empresas`.`id` = _id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_update_estados` (IN `nombre_estado` VARCHAR(100) CHARSET utf8, IN `descripcion` VARCHAR(100) CHARSET utf8, IN `updated_by` VARCHAR(50), IN `id` INT)  NO SQL
UPDATE `estados` SET `nombre_estado` = nombre_estado, `descripcion` = descripcion, `updated_at` = CURRENT_TIMESTAMP, `updated_by` = updated_by WHERE `estados`.`id` = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_update_tipo_usuario` (IN `id` BIGINT, IN `nombre` VARCHAR(50) CHARSET utf8, IN `descripcion` VARCHAR(100) CHARSET utf8, IN `empresa` BIGINT, IN `updated_by` VARCHAR(50) CHARSET utf8)  NO SQL
UPDATE `tipo_usuario` SET `nombre` = nombre, `descripcion` = descripcion, `empresa` = empresa, `updated_at` = CURRENT_TIMESTAMP WHERE `tipo_usuario`.`id` = id$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresas`
--

CREATE TABLE `empresas` (
  `id` bigint(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `web` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `activo` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `empresas`
--

INSERT INTO `empresas` (`id`, `nombre`, `direccion`, `web`, `created_at`, `created_by`, `updated_at`, `updated_by`, `activo`) VALUES
(9, 'mi nueva empresa', NULL, NULL, '2019-11-02 17:36:13', '', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados`
--

CREATE TABLE `estados` (
  `id` bigint(11) NOT NULL,
  `nombre_estado` varchar(50) NOT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `empresa` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `activo` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estados`
--

INSERT INTO `estados` (`id`, `nombre_estado`, `descripcion`, `empresa`, `created_at`, `created_by`, `updated_at`, `updated_by`, `activo`) VALUES
(1, 'inicial modificado puto', 'nooo pues si comente putooo', 9, '2019-11-06 01:31:29', 'admin', '2019-11-09 19:42:12', NULL, 1),
(2, 'entregado mod', 'entregado en recepcion modificado', 9, '2019-11-06 01:34:25', 'administrador', NULL, NULL, 1),
(4, 'nuevo estado sp mod', 'sp modificado', 9, '2019-11-09 18:45:14', 'jose', '2019-11-23 06:00:00', 'jose', 1),
(5, 'nueva mierda', 'mierda', 9, '2019-11-09 19:21:14', 'jose florian', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gestion_casos`
--

CREATE TABLE `gestion_casos` (
  `id` bigint(11) NOT NULL,
  `tipo_casos` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `estado` int(11) NOT NULL,
  `usuario_asignado` int(11) NOT NULL,
  `usuario_creador` int(11) NOT NULL,
  `documento` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `activo` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historico_gestion_casos`
--

CREATE TABLE `historico_gestion_casos` (
  `id` int(11) NOT NULL,
  `gestion_casos` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  `usuario_asignado` int(11) NOT NULL,
  `usuario_creador` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `accion` varchar(25) DEFAULT NULL,
  `observaciones` varchar(100) DEFAULT NULL,
  `activo` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 2),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 2),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 2),
(6, '2016_06_01_000004_create_oauth_clients_table', 2),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'oEt9EazNSNWFBgJdtmnZhLMpwbB7YHrjgfR63DKK', 'http://localhost', 1, 0, 0, '2019-10-29 10:35:43', '2019-10-29 10:35:43'),
(2, NULL, 'Laravel Password Grant Client', 'hbSKLn7pUUwnfQcJNrepprvLK5aXA3KFWjPgYx74', 'http://localhost', 0, 1, 0, '2019-10-29 10:35:44', '2019-10-29 10:35:44');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-10-29 10:35:43', '2019-10-29 10:35:43');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paises`
--

CREATE TABLE `paises` (
  `id` int(11) NOT NULL,
  `iso` char(2) DEFAULT NULL,
  `nombre` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `paises`
--

INSERT INTO `paises` (`id`, `iso`, `nombre`) VALUES
(1, 'AF', 'Afganistán'),
(2, 'AX', 'Islas Gland'),
(3, 'AL', 'Albania'),
(4, 'DE', 'Alemania'),
(5, 'AD', 'Andorra'),
(6, 'AO', 'Angola'),
(7, 'AI', 'Anguilla'),
(8, 'AQ', 'Antártida'),
(9, 'AG', 'Antigua y Barbuda'),
(10, 'AN', 'Antillas Holandesas'),
(11, 'SA', 'Arabia Saudí'),
(12, 'DZ', 'Argelia'),
(13, 'AR', 'Argentina'),
(14, 'AM', 'Armenia'),
(15, 'AW', 'Aruba'),
(16, 'AU', 'Australia'),
(17, 'AT', 'Austria'),
(18, 'AZ', 'Azerbaiyán'),
(19, 'BS', 'Bahamas'),
(20, 'BH', 'Bahréin'),
(21, 'BD', 'Bangladesh'),
(22, 'BB', 'Barbados'),
(23, 'BY', 'Bielorrusia'),
(24, 'BE', 'Bélgica'),
(25, 'BZ', 'Belice'),
(26, 'BJ', 'Benin'),
(27, 'BM', 'Bermudas'),
(28, 'BT', 'Bhután'),
(29, 'BO', 'Bolivia'),
(30, 'BA', 'Bosnia y Herzegovina'),
(31, 'BW', 'Botsuana'),
(32, 'BV', 'Isla Bouvet'),
(33, 'BR', 'Brasil'),
(34, 'BN', 'Brunéi'),
(35, 'BG', 'Bulgaria'),
(36, 'BF', 'Burkina Faso'),
(37, 'BI', 'Burundi'),
(38, 'CV', 'Cabo Verde'),
(39, 'KY', 'Islas Caimán'),
(40, 'KH', 'Camboya'),
(41, 'CM', 'Camerún'),
(42, 'CA', 'Canadá'),
(43, 'CF', 'República Centroafricana'),
(44, 'TD', 'Chad'),
(45, 'CZ', 'República Checa'),
(46, 'CL', 'Chile'),
(47, 'CN', 'China'),
(48, 'CY', 'Chipre'),
(49, 'CX', 'Isla de Navidad'),
(50, 'VA', 'Ciudad del Vaticano'),
(51, 'CC', 'Islas Cocos'),
(52, 'CO', 'Colombia'),
(53, 'KM', 'Comoras'),
(54, 'CD', 'República Democrática del Congo'),
(55, 'CG', 'Congo'),
(56, 'CK', 'Islas Cook'),
(57, 'KP', 'Corea del Norte'),
(58, 'KR', 'Corea del Sur'),
(59, 'CI', 'Costa de Marfil'),
(60, 'CR', 'Costa Rica'),
(61, 'HR', 'Croacia'),
(62, 'CU', 'Cuba'),
(63, 'DK', 'Dinamarca'),
(64, 'DM', 'Dominica'),
(65, 'DO', 'República Dominicana'),
(66, 'EC', 'Ecuador'),
(67, 'EG', 'Egipto'),
(68, 'SV', 'El Salvador'),
(69, 'AE', 'Emiratos Árabes Unidos'),
(70, 'ER', 'Eritrea'),
(71, 'SK', 'Eslovaquia'),
(72, 'SI', 'Eslovenia'),
(73, 'ES', 'España'),
(74, 'UM', 'Islas ultramarinas de Estados Unidos'),
(75, 'US', 'Estados Unidos'),
(76, 'EE', 'Estonia'),
(77, 'ET', 'Etiopía'),
(78, 'FO', 'Islas Feroe'),
(79, 'PH', 'Filipinas'),
(80, 'FI', 'Finlandia'),
(81, 'FJ', 'Fiyi'),
(82, 'FR', 'Francia'),
(83, 'GA', 'Gabón'),
(84, 'GM', 'Gambia'),
(85, 'GE', 'Georgia'),
(86, 'GS', 'Islas Georgias del Sur y Sandwich del Sur'),
(87, 'GH', 'Ghana'),
(88, 'GI', 'Gibraltar'),
(89, 'GD', 'Granada'),
(90, 'GR', 'Grecia'),
(91, 'GL', 'Groenlandia'),
(92, 'GP', 'Guadalupe'),
(93, 'GU', 'Guam'),
(94, 'GT', 'Guatemala'),
(95, 'GF', 'Guayana Francesa'),
(96, 'GN', 'Guinea'),
(97, 'GQ', 'Guinea Ecuatorial'),
(98, 'GW', 'Guinea-Bissau'),
(99, 'GY', 'Guyana'),
(100, 'HT', 'Haití'),
(101, 'HM', 'Islas Heard y McDonald'),
(102, 'HN', 'Honduras'),
(103, 'HK', 'Hong Kong'),
(104, 'HU', 'Hungría'),
(105, 'IN', 'India'),
(106, 'ID', 'Indonesia'),
(107, 'IR', 'Irán'),
(108, 'IQ', 'Iraq'),
(109, 'IE', 'Irlanda'),
(110, 'IS', 'Islandia'),
(111, 'IL', 'Israel'),
(112, 'IT', 'Italia'),
(113, 'JM', 'Jamaica'),
(114, 'JP', 'Japón'),
(115, 'JO', 'Jordania'),
(116, 'KZ', 'Kazajstán'),
(117, 'KE', 'Kenia'),
(118, 'KG', 'Kirguistán'),
(119, 'KI', 'Kiribati'),
(120, 'KW', 'Kuwait'),
(121, 'LA', 'Laos'),
(122, 'LS', 'Lesotho'),
(123, 'LV', 'Letonia'),
(124, 'LB', 'Líbano'),
(125, 'LR', 'Liberia'),
(126, 'LY', 'Libia'),
(127, 'LI', 'Liechtenstein'),
(128, 'LT', 'Lituania'),
(129, 'LU', 'Luxemburgo'),
(130, 'MO', 'Macao'),
(131, 'MK', 'ARY Macedonia'),
(132, 'MG', 'Madagascar'),
(133, 'MY', 'Malasia'),
(134, 'MW', 'Malawi'),
(135, 'MV', 'Maldivas'),
(136, 'ML', 'Malí'),
(137, 'MT', 'Malta'),
(138, 'FK', 'Islas Malvinas'),
(139, 'MP', 'Islas Marianas del Norte'),
(140, 'MA', 'Marruecos'),
(141, 'MH', 'Islas Marshall'),
(142, 'MQ', 'Martinica'),
(143, 'MU', 'Mauricio'),
(144, 'MR', 'Mauritania'),
(145, 'YT', 'Mayotte'),
(146, 'MX', 'México'),
(147, 'FM', 'Micronesia'),
(148, 'MD', 'Moldavia'),
(149, 'MC', 'Mónaco'),
(150, 'MN', 'Mongolia'),
(151, 'MS', 'Montserrat'),
(152, 'MZ', 'Mozambique'),
(153, 'MM', 'Myanmar'),
(154, 'NA', 'Namibia'),
(155, 'NR', 'Nauru'),
(156, 'NP', 'Nepal'),
(157, 'NI', 'Nicaragua'),
(158, 'NE', 'Níger'),
(159, 'NG', 'Nigeria'),
(160, 'NU', 'Niue'),
(161, 'NF', 'Isla Norfolk'),
(162, 'NO', 'Noruega'),
(163, 'NC', 'Nueva Caledonia'),
(164, 'NZ', 'Nueva Zelanda'),
(165, 'OM', 'Omán'),
(166, 'NL', 'Países Bajos'),
(167, 'PK', 'Pakistán'),
(168, 'PW', 'Palau'),
(169, 'PS', 'Palestina'),
(170, 'PA', 'Panamá'),
(171, 'PG', 'Papúa Nueva Guinea'),
(172, 'PY', 'Paraguay'),
(173, 'PE', 'Perú'),
(174, 'PN', 'Islas Pitcairn'),
(175, 'PF', 'Polinesia Francesa'),
(176, 'PL', 'Polonia'),
(177, 'PT', 'Portugal'),
(178, 'PR', 'Puerto Rico'),
(179, 'QA', 'Qatar'),
(180, 'GB', 'Reino Unido'),
(181, 'RE', 'Reunión'),
(182, 'RW', 'Ruanda'),
(183, 'RO', 'Rumania'),
(184, 'RU', 'Rusia'),
(185, 'EH', 'Sahara Occidental'),
(186, 'SB', 'Islas Salomón'),
(187, 'WS', 'Samoa'),
(188, 'AS', 'Samoa Americana'),
(189, 'KN', 'San Cristóbal y Nevis'),
(190, 'SM', 'San Marino'),
(191, 'PM', 'San Pedro y Miquelón'),
(192, 'VC', 'San Vicente y las Granadinas'),
(193, 'SH', 'Santa Helena'),
(194, 'LC', 'Santa Lucía'),
(195, 'ST', 'Santo Tomé y Príncipe'),
(196, 'SN', 'Senegal'),
(197, 'CS', 'Serbia y Montenegro'),
(198, 'SC', 'Seychelles'),
(199, 'SL', 'Sierra Leona'),
(200, 'SG', 'Singapur'),
(201, 'SY', 'Siria'),
(202, 'SO', 'Somalia'),
(203, 'LK', 'Sri Lanka'),
(204, 'SZ', 'Suazilandia'),
(205, 'ZA', 'Sudáfrica'),
(206, 'SD', 'Sudán'),
(207, 'SE', 'Suecia'),
(208, 'CH', 'Suiza'),
(209, 'SR', 'Surinam'),
(210, 'SJ', 'Svalbard y Jan Mayen'),
(211, 'TH', 'Tailandia'),
(212, 'TW', 'Taiwán'),
(213, 'TZ', 'Tanzania'),
(214, 'TJ', 'Tayikistán'),
(215, 'IO', 'Territorio Británico del Océano Índico'),
(216, 'TF', 'Territorios Australes Franceses'),
(217, 'TL', 'Timor Oriental'),
(218, 'TG', 'Togo'),
(219, 'TK', 'Tokelau'),
(220, 'TO', 'Tonga'),
(221, 'TT', 'Trinidad y Tobago'),
(222, 'TN', 'Túnez'),
(223, 'TC', 'Islas Turcas y Caicos'),
(224, 'TM', 'Turkmenistán'),
(225, 'TR', 'Turquía'),
(226, 'TV', 'Tuvalu'),
(227, 'UA', 'Ucrania'),
(228, 'UG', 'Uganda'),
(229, 'UY', 'Uruguay'),
(230, 'UZ', 'Uzbekistán'),
(231, 'VU', 'Vanuatu'),
(232, 'VE', 'Venezuela'),
(233, 'VN', 'Vietnam'),
(234, 'VG', 'Islas Vírgenes Británicas'),
(235, 'VI', 'Islas Vírgenes de los Estados Unidos'),
(236, 'WF', 'Wallis y Futuna'),
(237, 'YE', 'Yemen'),
(238, 'DJ', 'Yibuti'),
(239, 'ZM', 'Zambia'),
(240, 'ZW', 'Zimbabue');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_casos`
--

CREATE TABLE `tipo_casos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `empresa` int(11) NOT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `posicion` int(11) NOT NULL,
  `es_fin` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `activo` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_usuario`
--

CREATE TABLE `tipo_usuario` (
  `id` int(11) NOT NULL,
  `empresa` bigint(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `activo` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_usuario`
--

INSERT INTO `tipo_usuario` (`id`, `empresa`, `nombre`, `descripcion`, `created_at`, `created_by`, `updated_at`, `updated_by`, `activo`) VALUES
(1, 9, 'admini', 'administradores ', '2019-11-06 01:13:29', 'user', NULL, NULL, 1),
(2, 9, 'user', 'usuario normal', '2019-11-06 01:25:36', '', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nombres` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `apellidos` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `direccion` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `pais` bigint(20) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tipo_usuario` bigint(20) NOT NULL,
  `empresa` bigint(20) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `nombres`, `apellidos`, `direccion`, `pais`, `email`, `password`, `tipo_usuario`, `empresa`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'jose florian', '', '', '', 1, 'cjfn10101@gmail.com', '$2y$10$k.Z2a9VlwYBA.frKbyEAn.wUZ3mXn99DM2Q1r6lZiOp.cwY6BnHC.', 1, 9, 'dKORG1SzcNPPYwO6oGpgseYuyhjJ8mMGRevwAmuXdk1gVtN9Q6kKfP8EN9Lg', '2019-10-29 06:22:02', '2019-10-29 08:47:06'),
(2, 'user123', '', '', '', 8, 'user123@gmail.com', 'user6df5d4f56sd4fs5df4ds5f45ds', 2, 9, NULL, NULL, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `empresas`
--
ALTER TABLE `empresas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `estados`
--
ALTER TABLE `estados`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `gestion_casos`
--
ALTER TABLE `gestion_casos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `historico_gestion_casos`
--
ALTER TABLE `historico_gestion_casos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`),
  ADD KEY `oauth_access_tokens_client_id_index` (`client_id`);

--
-- Indices de la tabla `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indices de la tabla `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indices de la tabla `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indices de la tabla `paises`
--
ALTER TABLE `paises`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indices de la tabla `tipo_casos`
--
ALTER TABLE `tipo_casos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_usuario`
--
ALTER TABLE `tipo_usuario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `empresas`
--
ALTER TABLE `empresas`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `estados`
--
ALTER TABLE `estados`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `gestion_casos`
--
ALTER TABLE `gestion_casos`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `historico_gestion_casos`
--
ALTER TABLE `historico_gestion_casos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `paises`
--
ALTER TABLE `paises`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=241;

--
-- AUTO_INCREMENT de la tabla `tipo_casos`
--
ALTER TABLE `tipo_casos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tipo_usuario`
--
ALTER TABLE `tipo_usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
