@extends('layouts.appprincipal')



@section('content')

<meta name="csrf-token" content="{{ csrf_token() }}" />



<div class="container">
   <div class="row">
      <div class="col-md-2"></div>
          <div class="col-md-10">
            <div class="panel panel-default">
            <div class="panel panel-primary">
                <div class="panel-heading"><strong>Gestion de casos</strong></div>
              </div>
            <div class="panel-body">
            <h2>Cree y administre nuevas gestiones de casos y seguimiento de estos</h2>
            
              @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>{{ $message }}</strong>
                </div>
              @endif
    <div class="table-responsive">
  @if(Auth::user()->tipo_usuario=='1')
       <span class="btn btn-primary" data-toggle="modal" data-target="#addModal">Agregar <span class="fa fa-plus"></span></span>
  @endif
        
        <table id="myDatatable" >
          <tr>
            <th>
              id
            </th>
            <th>
              nombre
            </th>          
           
            <th>
              descripcion
            </th>
             <th>
              nombre_estado
            </th>
             <th>
              usuario_encargado
            </th>
            <th>
              nombre_oficina
            </th>
            <th>
              created_at
            </th>
          </tr>
        </table>
  
  </div>


      <!-- Mostrar detalle -->
            <!-- Modal -->
            <div id="editModal" class="modal fade" role="dialog">
              <div class="modal-dialog">
              <!-- Contenido del modal -->
              <div class="modal-content">
                <div class="modal-header">Cambiar de estado
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                <form  action="" method="post"  > 
                 <div class="form-group">
                   <input type="hidden" class="form-control" id="edit_codigo" name="edit_codigo">
                    
                  </div>
                   <div class="form-group">
                    <label for="last_name">Estado:</label>
                     <select id="estado" name="estado" class="form-control" >
                      <option value="">Seleccione nuevo estado...</option>
                     </select>

                  </div> 
                  <div class="form-group">
                    <label for="last_name">Observaciones:</label>
                    <input type="text" class="form-control" id="edit_observaciones" name="edit_observaciones">
                  </div>
                  
               
                </form>
                     
                </div>
               <div class="modal-footer">
                <button class="btn btn-default" onclick="funUpdate()"> Modificar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-window-close"></span>Cerrar</button>
              </div>
              </div>
              </div>
            </div>


<!-- Mostrar detalle -->
            <!-- Modal -->
            <div id="adjuntaModal" class="modal fade" role="dialog">
              <div class="modal-dialog">
              <!-- Contenido del modal -->
              <div class="modal-content">
                <div class="modal-header">Adjunta documento
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                <form method="POST" action="{{ url('gestion_casos/save') }}" accept-charset="UTF-8" enctype="multipart/form-data">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                 <input type="text" readOnly="readOnly" class="form-control" id="edit_codigo_adjunta" name="edit_codigo_adjunta">
                    
                <div class="form-group">
                  <label class="col-md-4 control-label">Nuevo Archivo</label>
                  <div class="col-md-6">
                    <input type="file" class="form-control" name="file" >
                  </div>
                </div>
     
                <div class="form-group">
                  <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">Enviar</button>
                  </div>
                </div>
              </form>
                     
                </div>
               <div class="modal-footer">
                <button class="btn btn-default" onclick="funUpdate()"> Modificar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-window-close"></span>Cerrar</button>
              </div>
              </div>
              </div>
            </div>

          <!-- Mostrar detalle -->
            <!-- Modal -->
            <div id="ShowDetalle" class="modal fade" role="dialog">
              <div class="modal-dialog">
              <!-- Contenido del modal -->
              <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                 <p><b>Codigo : </b><span id="view_codigo" class="text-success"></span></p>
                  <p><b>nombre de tipo gestion : </b><span id="view_nombre_tipo_gestion" class="text-success"></span></p>
                  <p><b>Descripcion : </b><span id="view_descripcion" class="text-success"></span></p>
                  <p><b>Estado : </b><span id="view_nombre_estado" class="text-success"></span></p>
                  <p><b>Usuario Asignado : </b><span id="view_nombre_usuario_asignado" class="text-success"></span></p>
                  <p><b>Oficina : </b><span id="view_nombre_oficina" class="text-success"></span></p>
                  <p><b>Documento : </b><span id="view_nombre_documento" class="text-success"></span></p>
                 <p><b>Fecha de creación : </b><span id="view_fecha_creacion" class="text-success"></span></p>
                  <p><b>Creado por : </b><span id="view_creado_por" class="text-success"></span></p>
                  <p><b>Modificado : </b><span id="view_modificado" class="text-success"></span></p>
                   <p><b>Fecha Modificacion : </b><span id="view_fecha_modificado" class="text-success"></span></p>

                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Cerrar</button>
                </div>
              </div>
              </div>
            </div>
    
        <!-- Add Modal start -->
        <div class="modal fade" id="addModal" role="dialog">
          <div class="modal-dialog">
            <form  action="" method="post"  > 
            <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
               
                 <div class="form-group">
                    <label for="last_name">Tipo de gestion:</label>
                     <select id="tipo_casos" name="tipo_casos" class="form-control" >
                      <option value="">Seleccione una...</option>
                     </select>
                  </div> 
                   <div class="form-group">
                    <label for="last_name">Usuario Asignado:</label>
                     <select id="usuario" name="usuario" class="form-control" >
                      <option value="">Seleccione una...</option>
                     </select>
                     

                  </div> 
                 
                   <div class="form-group">
                    <label for="last_name">Oficina:</label>
                     <select id="oficinas" name="oficinas" class="form-control" >
                      <option value="">Seleccione una...</option>
                     </select>

                  </div> 
                  
                  
                  
                </form>
              
              <div class="modal-footer">
                <span class="btn btn-default" onclick="fun_inserta()" onkeypress="fun_inserta()"> Nuevo</span>
                <button type="button" class="btn btn-default" data-dismiss="modal" tabindex="2"><span class="fa fa-window-close"></span>Cerrar</button>
              </div>
            </div>
          <input type="hidden" id="created_by" name="" value="{{Auth::user()->name}}">
          <input type="hidden" id="miempresa" name="" value="{{Auth::user()->empresa}}">
          <input type="hidden" id="updated_by" name="" value="{{Auth::user()->name}}">
          
          
        </div>
  
  
      
    

                <link href="{{ url('/') }}/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
                <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.15/css/jquery.dataTables.css">
                <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
                <script src="//code.jquery.com/jquery-2.1.3.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.js"></script>

                <script language="javascript" type="text/javascript">
                                    
                  var http = "http://"
                  var url = location.host;
                  var slash = "/";
                  var urlhttp = http + url + slash;
                  //var server = urlhttp;
                  var server = "http://50.116.47.187/gestion_casos/public";
              //alert(urlhttp+ " " + server);
        $(document).ready(function () {
        //alert({{Auth::user()->empresa}});

        $.ajax({
          url: server + '/api/gestion_casos',
          method: 'get',
          dataType: 'json',
          success: function (data) {
           console.log(data);
            $('#myDatatable').dataTable({
              paging: true,
              sort: true,
              searching: true,

              data: data,
              columns: [
                {
                  "data": "id", 'title': "Opciones", "width": "50px", "render": function (data, type, row) {
                    return '<a class="btn btn-default" data-toggle="modal" data-target="#ShowDetalle" onclick="fun_view(' + row.id + ')"><span class="fa fa-eye"></span>Ver Detalle</a><a class="btn btn-warning" href="#" data-toggle="modal" data-target="#adjuntaModal" title="Adjuntar archivo" onclick="fun_adjunta(' + row.id + ')"><span class="fa fa-file"></span> Adjuntar Archivo</a><a class="btn btn-primary" href="#" data-toggle="modal" data-target="#editModal" title="Editar estado" onclick="fun_edit(' + row.id + ')"><span class="fa fa-edit"></span> Modificar Estado</a><a class="btn btn-danger" href="#" title="Eliminar informacion"  onclick="fun_delete(' + row.id + ')"><span class="fa fa-close"></span>Eliminar</a>'            //return '<a class="popup"     href="/usuarios/editar' + data + '">Editar</a>';
                  }
                },
               
                { 'title': "nombre", 'data': 'nombre_tipo_casos' },
                { 'title': "Descripcion", 'data': 'descripcion' },

                { 'title': "nombre estado", 'data': 'nombre_estado' },
                { 'title': "Usuario Asignado", 'data': 'nombre_usuario_asignado' },
                { 'title': "Oficina", 'data': 'nombre_oficina' },
                { 'title': "created_at", 'data': 'created_at' },
                
              ],
              dom: 'Bfrtip',
              buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
              ],

              language: {
                "order": [[1, "desc"]],
                "zeroRecords": "Nada que mostrar",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Registros",
                "infoEmpty": "No hay registros disponibles",
                "infoFiltered": "(filtrados from _MAX_ total records)",
                "sSearch": "Buscar:",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Documentos",
                "loadingRecords": "Cargando...",
                "buttons": ['copy', 'excel', 'csv', 'pdf', 'print'],
                "processing": "Procesando...",
                "paginate": {
                  "first": "Primero",
                  "last": "Ultimo",
                  "next": "Siguiente",
                  "previous": "Anterior"
                }
              }
            });

          }
        });
        
        debugger;
        var empresa = $("#miempresa").val();

        $.ajax({
          type: "get",
           url: server + '/api/getOficinas/'+empresa,
          dataType: "json",
          success: function (response) {
            console.log(response);
            console.log(response[0].nombre);
            var i;
            //cargaSelect(response);
            for (i = 0; i < response.length; i++) {
              var option = $(document.createElement('option'));
              //alert(response[i].nombre);
              option.text(response[i].nombre);
              option.val(response[i].id);
              $("#oficinas").append(option);
            }
          },
          error: function (msg) {
            console.log(msg);
          }
        });

        $.ajax({
          type: "get",
           url: server + '/api/getEstados/'+empresa,
          dataType: "json",
          success: function (response) {
            console.log(response);
            console.log(response[0].nombre_estado);
            var i;
            //cargaSelect(response);
            for (i = 0; i < response.length; i++) {
              var option = $(document.createElement('option'));
              //alert(response[i].nombre);
              option.text(response[i].nombre_estado);
              option.val(response[i].id);
              $("#estado").append(option);
            }
          },
          error: function (msg) {
            $("#dvAlerta > span").text("Error al llenar el combo");
          }
        });
        $.ajax({
          type: "get",
           url: server + '/api/getTipoCasos/'+empresa,
          dataType: "json",
          success: function (response) {
            console.log(response);
            console.log(response[0].nombre);
            var i;
            //cargaSelect(response);
            for (i = 0; i < response.length; i++) {
              var option = $(document.createElement('option'));
              //alert(response[i].nombre);
              option.text(response[i].nombre);
              option.val(response[i].id);
              $("#tipo_casos").append(option);
            }
          },
          error: function (msg) {
            $("#dvAlerta > span").text("Error al llenar el combo");
          }
        });
          $.ajax({
          type: "get",
           url: server + '/api/getUsuarios/'+empresa,
          dataType: "json",
          success: function (response) {
            console.log(response);
            console.log(response[0].name);
            var i;
            //cargaSelect(response);
            for (i = 0; i < response.length; i++) {
              var option = $(document.createElement('option'));
              //alert(response[i].nombre);
              option.text(response[i].name);
              option.val(response[i].id);
              $("#usuario").append(option);
            }
          },
          error: function (msg) {
            $("#dvAlerta > span").text("Error al llenar el combo");
          }
        });
        
      });

          function fun_inserta() {
        //alert("intento insertar");
        var view_url = server + '/api/gestion_casos';
        var tipo_casos = $("#tipo_casos").val();
        var estados = 1;
        var usuario = $("#usuario").val();
        var oficinas = $("#oficinas").val();
        //var nombre_usuario = $("#nombre_usuario").val();
        var created_by = $("#created_by").val();


       // alert(nombres + " " + apellidos + " " + direccion + " " + pais + " " + email  + " " + password + " " + tipo_usuario +" "+empresa);
        //alert(id);
        //alert(nombre+' '+direccion+' '+web);
        alert(tipo_casos+ ' '+estados+ ' '+usuario+ ' '+oficinas+ ' '+created_by);
        $.ajax({
          url: view_url,
          type: "POST",
          datatype: "json",
          data: {
            "tipo_caso": tipo_casos,
            "estado": estados,
            "usuario_asignado": usuario,
            "oficina":oficinas,
            "created_by":created_by
            },
          success: function (response) {
            //alert(obj.nombre);
            alert(response.respuesta);
            //console.log(response);
            location.reload();
            //alert(result.data.nombre);
          },
          error: function () {
            alert(response.error);
          }
        });
      }


 
    function fun_view(id)
    {
      //alert(id);
      var view_url = server + '/api/gestion_casos/'+id;
      $.ajax({
        url: view_url,
        type:"GET", 
        success: function(result){
            var str = result;
            console.log(result);
            console.log(result[0].nombre_estado);
                        //alert(result.data.nombre);
                        //alert(result.data.nombre);
          //alert(id);
          $("#view_codigo").text(result[0].id);
          $("#view_nombre_tipo_gestion").text(result[0].nombre_tipo_casos);
          $("#view_descripcion").text(result[0].descripcion);
          $("#view_nombre_estado").text(result[0].nombre_estado);
          $("#view_nombre_usuario_asignado").text(result[0].nombre_estado);
          $("#view_nombre_oficina").text(result[0].nombre_oficina);
          $("#view_nombre_documento").text(result[0].documento);
          $("#view_nombre_documento").text(result[0].documento);
          $("#view_fecha_creacion").text(result[0].created_at);
          $("#view_creado_por").text(result[0].created_by);
          
          
          $("#view_updated").text(result.updated_at);
          

        },
        error: function (result) {
            alert(result);
          }
      });
    }

    function fun_adjunta(id)
    {
      //alert(id);
      //debugger;
      var view_url = server + '/api/gestion_casos/'+id;
      $.ajax({
        url: view_url,
        type:"GET", 
        success: function(result){
            var str = result;
            console.log(result);
            console.log(result[0].nombre_estado);
                        //alert(result.data.nombre);
                        //alert(result.data.nombre);
          //alert(id);
          
          $("#edit_codigo_adjunta").text(result[0].id);
        

        },
        error: function (result) {
            alert(result);
          }
      });
    }
 
 
    function fun_edit(id)
    {
      //alert(id);
      var view_url = server + '/api/gestion_casos/'+id;
      $.ajax({
        url: view_url,
        type:"GET", 
        success: function(result){
            var str = result;
            console.log(result);
            //console.log(result[0].nombre_estado);
                        //alert(result.data.nombre);
                        //alert(result.data.nombre);
          //alert(id);
          // alert(result);
          $("#edit_codigo").val(result[0].id);
          $("#edit_nombre_estado").val(result[0].nombre_estado);
          $("#view_descripcion").val(result[0].descripcion);
         },
          error: function (jqXHR, exception) {
          var msg = '';
          if (jqXHR.status === 0) {
              msg = 'Not connect.\n Verify Network.';
          } else if (jqXHR.status == 404) {
              msg = 'Requested page not found. [404]';
          } else if (jqXHR.status == 500) {
              msg = 'Internal Server Error [500].';
          } else if (exception === 'parsererror') {
              msg = 'Requested JSON parse failed.';
          } else if (exception === 'timeout') {
              msg = 'Time out error.';
          } else if (exception === 'abort') {
              msg = 'Ajax request aborted.';
          } else {
              msg = 'Uncaught Error.\n' + jqXHR.responseText;
          }
          $('#post').html(msg);
      },
      });
    }

 
  function funUpdate() {
        //alert("intento insertar");
        debugger;
        var id= $("#edit_codigo").val();
        var view_url = server + '/api/gestion_casos/';
        var estado = $("#estado").val();
        //var descripcion = $("#edit_descripcion").val();

        var updated_by = $("#updated_by").val();
        //alert(codigo_banco + " " + telefono + " " + nombre + " " + observaciones + " " + web + " " + observaciones + " " + modified_by + " " + token);
        //alert(nombre);
        //alert(id);
        console.log(id+" "+estado+" "+descripcion+" "+updated_by+" "+view_url);
       
        $.ajax({
          url: view_url,
          type: "PUT",
          datatype: "json",   
          data: {
            "estado": estado,
            "updated_by": updated_by,
            "id":id
            
          },
          success: function (response) {
            //alert(obj.nombre);
            console.log(response);
            location.reload();
            //alert(result.data.nombre);
          },
          error: function (response) {
            alert(response);
          }
        });
      }

    function fun_delete(id)
    {
      var conf = confirm("Esta seguro que desea eliminar No."+id+"??");
      if(conf){
        var delete_url = server + '/api/gestion_casos/'+id;
        $.ajax({
          url: delete_url,
          type:"DELETE",  
          success: function(response){
            alert(response);
            location.reload(); 
          }
        });
      }
      else{
        return false;
      }
    }
 </script>       
          </div>
          <!-- /.col-lg-12 -->
      </div>
</div>

@endsection
