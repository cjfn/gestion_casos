@extends('layouts.appprincipal')



@section('content')

<meta name="csrf-token" content="{{ csrf_token() }}" />



<div class="container">
    <div class="row">
      <div class="col-md-2"></div>
          <div class="col-md-10">
            <div class="panel panel-default">
            <div class="panel panel-primary">
                <div class="panel-heading"><strong>Tipo Caso</strong></div>
              </div>
            <div class="panel-body">
            <h2>Cree nuevos tipos de casos para usted y su empresa</h2>
            
              @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>{{ $message }}</strong>
                </div>
              @endif
    <div class="table-responsive">
  @if(Auth::user()->tipo_usuario=='1')
       <span class="btn btn-primary" data-toggle="modal" data-target="#addModal">Agregar <span class="fa fa-plus"></span></span>
  @endif
        
        <table id="myDatatable" >
          <tr>
            <th>
              id
            </th>
            <th>
              nombre
            </th>          
           
            <th>
              descripcion
            </th>
             <th>
              nombre_empresa
            </th>
         
            <th>
              created_at
            </th>
          </tr>
        </table>
  
  </div>


      <!-- Mostrar detalle -->
            <!-- Modal -->
            <div id="editModal" class="modal fade" role="dialog">
              <div class="modal-dialog">
              <!-- Contenido del modal -->
              <div class="modal-content">
                <div class="modal-header">editar datos
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                <form  action="" method="post"  > 
                 <div class="form-group">
                   <input type="hidden" class="form-control" id="edit_codigo" name="edit_codigo">
                    <label for="last_name">Nombre de tipo de caso:</label>
                    <input type="text" class="form-control" id="edit_nombre" name="edit_nombre">
                  </div>
                  <div class="form-group">
                    <label for="last_name">Descripcion:</label>
                    <input type="text" class="form-control" id="edit_descripcion" name="edit_direccion">
                  </div>
                  
               
                </form>
                     
                </div>
               <div class="modal-footer">
                <button class="btn btn-default" onclick="funUpdate()"> Modificar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-window-close"></span>Cerrar</button>
              </div>
              </div>
              </div>
            </div>

          <!-- Mostrar detalle -->
            <!-- Modal -->
            <div id="ShowDetalle" class="modal fade" role="dialog">
              <div class="modal-dialog">
              <!-- Contenido del modal -->
              <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                 <p><b>Codigo : </b><span id="view_codigo" class="text-success"></span></p>
                  <p><b>nombre de estado : </b><span id="view_nombre_estado" class="text-success"></span></p>
                  <p><b>Descripcion : </b><span id="view_descripcion" class="text-success"></span></p>
                  <p><b>Empresa asignada : </b><span id="view_nombre_empresa" class="text-success"></span></p>
                 <p><b>Fecha de creación : </b><span id="view_fecha_creacion" class="text-success"></span></p>
                  <p><b>Creado por : </b><span id="view_creado_por" class="text-success"></span></p>
                  <p><b>Modificado : </b><span id="view_modificado" class="text-success"></span></p>
                   <p><b>Fecha Modificacion : </b><span id="view_fecha_modificado" class="text-success"></span></p>

                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Cerrar</button>
                </div>
              </div>
              </div>
            </div>
    
        <!-- Add Modal start -->
        <div class="modal fade" id="addModal" role="dialog">
          <div class="modal-dialog">
            <form  action="" method="post"  > 
            <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                <div class="form-group">
                    <label for="last_name">Nombre:</label>
                    <input type="text" class="form-control" id="nombre" name="nombre">
                  </div>
                   <div class="form-group">
                    <label for="last_name">Descripcion:</label>
                    <input type="text" class="form-control" id="descripcion" name="descripcion">
                  </div>
                   
                   <div class="form-group">
                    <label for="last_name">Empresa:</label>
                     <select id="empresa" name="empresa" class="form-control" >
                      <option value="">Seleccione una...</option>
                     </select>

                  </div> 
                  
                </form>
              
              <div class="modal-footer">
                <span class="btn btn-default" onclick="fun_inserta()" onkeypress="fun_inserta()"> Nuevo</span>
                <button type="button" class="btn btn-default" data-dismiss="modal" tabindex="2"><span class="fa fa-window-close"></span>Cerrar</button>
              </div>
            </div>
          <input type="hidden" id="created_by" name="" value="{{Auth::user()->name}}">
          <input type="hidden" id="empresa" name="" value="{{Auth::user()->empresa}}">
          
        </div>
  
      
    

                <link href="{{ url('/') }}/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
                <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.15/css/jquery.dataTables.css">
                <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
                <script src="//code.jquery.com/jquery-2.1.3.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.js"></script>

                <script language="javascript" type="text/javascript">
                                    
                  var http = "http://"
                  var url = location.host;
                  var slash = "/";
                  var urlhttp = http + url + slash;
                  //var server = urlhttp;
                  var server = "http://50.116.47.187/gestion_casos/public";
              //alert(urlhttp+ " " + server);
        $(document).ready(function () {
        //alert({{Auth::user()->empresa}});

        $.ajax({
          url: server + '/api/tipo_casos',
          method: 'get',
          dataType: 'json',
          success: function (data) {
            $('#myDatatable').dataTable({
              paging: true,
              sort: true,
              searching: true,

              data: data,
              columns: [
                {
                  "data": "id", 'title': "Opciones", "width": "50px", "render": function (data, type, row) {
                    return '<a class="btn btn-default" data-toggle="modal" data-target="#ShowDetalle" onclick="fun_view(' + row.id + ')"><span class="fa fa-eye"></span>Ver Detalle</a><a class="btn btn-warning" href="#" data-toggle="modal" data-target="#editModal" title="Editar informacion" onclick="fun_edit(' + row.id + ')"><span class="fa fa-edit"></span> Editar</a><a class="btn btn-danger" href="#" title="Eliminar informacion"  onclick="fun_delete(' + row.id + ')"><span class="fa fa-close"></span>Eliminar</a>'            //return '<a class="popup"     href="/usuarios/editar' + data + '">Editar</a>';
                  }
                },
               
                { 'title': "nombre tipo de caso", 'data': 'nombre' },
                { 'title': "Descripcion", 'data': 'descripcion' },
                { 'title': "empresa", 'data': 'nombre_empresa' },
                { 'title': "created_at", 'data': 'created_at' },
                
              ],
              dom: 'Bfrtip',
              buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
              ],

              language: {
                "order": [[1, "desc"]],
                "zeroRecords": "Nada que mostrar",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Registros",
                "infoEmpty": "No hay registros disponibles",
                "infoFiltered": "(filtrados from _MAX_ total records)",
                "sSearch": "Buscar:",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Documentos",
                "loadingRecords": "Cargando...",
                "buttons": ['copy', 'excel', 'csv', 'pdf', 'print'],
                "processing": "Procesando...",
                "paginate": {
                  "first": "Primero",
                  "last": "Ultimo",
                  "next": "Siguiente",
                  "previous": "Anterior"
                }
              }
            });
          }
        });

        
        var view_url = server + '/api/empresas';
        $.ajax({
          type: "GET",
          url: view_url,
          dataType: "json",
          success: function (response) {
            console.log(response);
            var i;
            //cargaSelect(response);
            for (i = 0; i < response.length; i++) {
              var option = $(document.createElement('option'));
              //alert(response[i].nombre);
              option.text(response[i].nombre);
              option.val(response[i].id);
              $("#empresa").append(option);
            }
          },
          error: function (msg) {
            $("#dvAlerta > span").text("Error al llenar el combo");
          }
        });

        
      });

          function fun_inserta() {
        //alert("intento insertar");
        var view_url = server + '/api/tipo_casos';
        var nombre = $("#nombre").val();
        var descripcion = $("#descripcion").val();
        var empresa = $("#empresa").val();
        var created_by = $("#created_by").val();

       console.log(nombre+' '+descripcion+' '+empresa+' '+created_by)
        //alert(id);
        //alert(nombre+' '+direccion+' '+web);
        $.ajax({
          url: view_url,
          type: "POST",
          datatype: "json",
          data: {
            "nombre": nombre,
            "empresa": empresa,
            "descripcion": descripcion,
            "created_by":created_by
            },
          success: function (response) {
            //alert(obj.nombre);
            alert(response.respuesta);
            console.log(response);
            location.reload();
            //alert(result.data.nombre);
          },
          error: function () {
            alert("error en peticion");
          }
        });
      }


 
    function fun_view(id)
    {
      //alert(id);
      var view_url = server + '/api/tipo_casos/'+id;
      $.ajax({
        url: view_url,
        type:"GET", 
        success: function(result){
            var str = result;
            console.log(result);
            console.log(result[0].nombre);
                        //alert(result.data.nombre);
                        //alert(result.data.nombre);
          //alert(id);
          $("#view_codigo").text(result[0].id);
          $("#view_nombre").text(result[0].nombre_estado);
          $("#view_descripcion").text(result[0].descripcion);
          $("#view_nombre_empresa").text(result[0].nombre_empresa);
          $("#view_fecha_creacion").text(result[0].created_at);
          $("#view_updated").text(result.updated_at);
          

        },
        error: function (result) {
            alert(result);
          }
      });
    }
 
    function fun_edit(id)
    {
      //alert(id);
      var view_url = server + '/api/tipo_casos/'+id;
      $.ajax({
        url: view_url,
        type:"GET", 
        success: function(result){
            var str = result;
            console.log(result);
            //console.log(result[0].nombre_estado);
                        //alert(result.data.nombre);
                        //alert(result.data.nombre);
          //alert(id);
          // alert(result);
          $("#edit_codigo").val(result[0].id);
          $("#edit_nombre").val(result[0].nombre);
          $("#view_descripcion").val(result[0].descripcion);
         },
          error: function (jqXHR, exception) {
          var msg = '';
          if (jqXHR.status === 0) {
              msg = 'Not connect.\n Verify Network.';
          } else if (jqXHR.status == 404) {
              msg = 'Requested page not found. [404]';
          } else if (jqXHR.status == 500) {
              msg = 'Internal Server Error [500].';
          } else if (exception === 'parsererror') {
              msg = 'Requested JSON parse failed.';
          } else if (exception === 'timeout') {
              msg = 'Time out error.';
          } else if (exception === 'abort') {
              msg = 'Ajax request aborted.';
          } else {
              msg = 'Uncaught Error.\n' + jqXHR.responseText;
          }
          $('#post').html(msg);
      },
      });
    }

 
  function funUpdate() {
        //alert("intento insertar");
        var id= $("#edit_codigo").val();
        var view_url = server + '/api/tipo_casos/'+id;
        var nombre = $("#edit_nombre").val();
        var descripcion = $("#edit_descripcion").val();
        var empresa = $("#empresa").val();
        var updated_by = $("#updated_by").val();
        //alert(codigo_banco + " " + telefono + " " + nombre + " " + observaciones + " " + web + " " + observaciones + " " + modified_by + " " + token);
        //alert(nombre);
        //alert(id);
       
        $.ajax({
          url: view_url,
          type: "PUT",
          datatype: "json",   
          data: {
            "nombre": nombre,
            "descripcion": descripcion,
            "updated_by": updated_by,
            "id":id
            
          },
          success: function (response) {
            //alert(obj.nombre);
            console.log(response);
            alert(response.respuesta);
            location.reload();
            //alert(result.data.nombre);
          },
          error: function (response) {
            alert(response);
          }
        });
      }

    function fun_delete(id)
    {
      var conf = confirm("Esta seguro que desea eliminar No."+id+"??");
      if(conf){
        var delete_url = server + '/api/tipo_casos/'+id;
        $.ajax({
          url: delete_url,
          type:"DELETE",  
          success: function(response){
            //alert(response);
            alert(response.respuesta);
            location.reload(); 
          }
        });
      }
      else{
        return false;
      }
    }
 </script>       
          </div>
          <!-- /.col-lg-12 -->
      </div>
</div>

@endsection
