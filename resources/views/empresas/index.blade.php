@extends('layouts.appprincipal')



@section('content')

<meta name="csrf-token" content="{{ csrf_token() }}" />



<div class="container">
    <div class="row">
      <div class="col-md-2"></div>
          <div class="col-md-10">
            <div class="panel panel-default">
            <div class="panel panel-primary">
                <div class="panel-heading"><strong>Empresas</strong></div>
              </div>
            <div class="panel-body">
            <h2>Administra empresas de tus clientes</h2>
            
              @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>{{ $message }}</strong>
                </div>
              @endif
    <div class="table-responsive">
  @if(Auth::user()->tipo_usuario=='1')
       <span class="btn btn-primary" data-toggle="modal" data-target="#addModal">Agregar <span class="fa fa-plus"></span></span>
  @endif
        
        <table id="myDatatable" >
          <tr>
            <th>
              nombre
            </th>
            <th>
              direccion
            </th>          
            <th>
              web
            </th>
            <th>
              creada
            </th>

            <th>
              created_at
            </th>
          </tr>
        </table>
  
  </div>


      <!-- Mostrar detalle -->
            <!-- Modal -->
            <div id="editModal" class="modal fade" role="dialog">
              <div class="modal-dialog">
              <!-- Contenido del modal -->
              <div class="modal-content">
                <div class="modal-header">editar datos
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                <form  action="" method="post"  > 
                 <div class="form-group">
                   <input type="hidden" class="form-control" id="edit_codigo" name="edit_codigo">
                    <label for="last_name">Nombre:</label>
                    <input type="text" class="form-control" id="edit_nombre" name="edit_nombre">
                  </div>
                  <div class="form-group">
                    <label for="last_name">Direccion:</label>
                    <input type="text" class="form-control" id="edit_direccion" name="edit_direccion">
                  </div>
                  <div class="form-group">
                    <label for="last_name">Página Web:</label>
                    <input type="text" class="form-control" id="edit_web" name="edit_web">
                  </div>
               
                </form>
                     
                </div>
               <div class="modal-footer">
                <button class="btn btn-default" onclick="funUpdate()"> Modificar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-window-close"></span>Cerrar</button>
              </div>
              </div>
              </div>
            </div>

          <!-- Mostrar detalle -->
            <!-- Modal -->
            <div id="ShowDetalle" class="modal fade" role="dialog">
              <div class="modal-dialog">
              <!-- Contenido del modal -->
              <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                 <p><b>Codigo : </b><span id="view_codigo" class="text-success"></span></p>
                  <p><b>Nombre : </b><span id="view_nombre" class="text-success"></span></p>
                  <p><b>Direccion : </b><span id="view_direccion" class="text-success"></span></p>
                  <p><b>Web : </b><span id="view_web" class="text-success"></span></p>
                 <p><b>Fecha de creación : </b><span id="view_fecha_creacion" class="text-success"></span></p>
                  <p><b>Creado por : </b><span id="view_creado_por" class="text-success"></span></p>
                  <p><b>Modificado : </b><span id="view_modificado" class="text-success"></span></p>
                   <p><b>Fecha Modificacion : </b><span id="view_fecha_modificado" class="text-success"></span></p>

                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Cerrar</button>
                </div>
              </div>
              </div>
            </div>
    
        <!-- Add Modal start -->
        <div class="modal fade" id="addModal" role="dialog">
          <div class="modal-dialog">
            <form  action="" method="post"  > 
            <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                <div class="form-group">
                   
                    <label for="last_name">Nombre:</label>
                    <input type="text" class="form-control" id="nombre" name="nombre">
                  </div>
                  <div class="form-group">
                    <label for="last_name">Direccion:</label>
                    <input type="text" class="form-control" id="direccion_" name="direccion_">
                  </div>
                  <div class="form-group">
                    <label for="last_name">Página Web:</label>
                    <input type="text" class="form-control" id="web" name="web">
                  </div>
                </form>
              
              <div class="modal-footer">
                <span class="btn btn-default" onclick="fun_inserta()" onkeypress="fun_inserta()" tabindex="3"> Nuevo</span>
                <button type="button" class="btn btn-default" data-dismiss="modal" tabindex="2"><span class="fa fa-window-close"></span>Cerrar</button>
              </div>
            </div>
          
          
        </div>
  
      
    

                <link href="{{ url('/') }}/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
                <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.15/css/jquery.dataTables.css">
                <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
                <script src="//code.jquery.com/jquery-2.1.3.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.js"></script>

                <script language="javascript" type="text/javascript">
                                    
                  var http = "http://"
                  var url = location.host;
                  var slash = "/";
                  var urlhttp = http + url + slash;
                  //var server = urlhttp;
                  var server = "http://50.116.47.187/gestion_casos/public";
              //alert(urlhttp+ " " + server);
        $(document).ready(function () {


        $.ajax({
          url: server + '/api/empresas',
          method: 'get',
          dataType: 'json',
          success: function (data) {
            $('#myDatatable').dataTable({
              paging: true,
              sort: true,
              searching: true,

              data: data,
              columns: [
                {
                  "data": "id", 'title': "Opciones", "width": "50px", "render": function (data, type, row) {
                    return '<a class="btn btn-default" data-toggle="modal" data-target="#ShowDetalle" onclick="fun_view(' + row.id + ')"><span class="fa fa-eye"></span>Ver Detalle</a><a class="btn btn-warning" href="#" data-toggle="modal" data-target="#editModal" title="Editar informacion" onclick="fun_edit(' + row.id + ')"><span class="fa fa-edit"></span> Editar</a><a class="btn btn-danger" href="#" title="Eliminar informacion"  onclick="fun_delete(' + row.id + ')"><span class="fa fa-close"></span>Eliminar</a>'            //return '<a class="popup"     href="/usuarios/editar' + data + '">Editar</a>';
                  }
                },
               
                { 'title': "nombre", 'data': 'nombre' },
                { 'title': "direccion", 'data': 'direccion' },
                { 'title': "web", 'data': 'web' },
                { 'title': "Fecha Creacion", 'data': 'created_at' },
                
              ],
              dom: 'Bfrtip',
              buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
              ],

              language: {
                "order": [[1, "desc"]],
                "zeroRecords": "Nada que mostrar",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Registros",
                "infoEmpty": "No hay registros disponibles",
                "infoFiltered": "(filtrados from _MAX_ total records)",
                "sSearch": "Buscar:",
                "thousands": ",",
                "buttons": ['copy', 'excel', 'csv', 'pdf', 'print'],
                "lengthMenu": "Mostrar _MENU_ Documentos",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "paginate": {
                  "first": "Primero",
                  "last": "Ultimo",
                  "next": "Siguiente",
                  "previous": "Anterior"
                }
              }
            });
          }
        });
        /*
        var view_url = server + 'servicios/ServiciosApp.asmx/GetTipoLotes';
        $.ajax({
          type: "post",
          url: view_url,
          dataType: "json",
          success: function (response) {
            console.log(response);
            console.log(response[0].nombre);
            var i;
            cargaSelect(response);
            for (i = 0; i < response.length; i++) {
              var option = $(document.createElement('option'));
              //alert(response[i].nombre);
              option.text(response[i].nombre);
              option.val(response[i].codigo_tipo_usuario);
              $("#tipo_usuario").append(option);
            }
          },
          error: function (msg) {
            $("#dvAlerta > span").text("Error al llenar el combo");
          }
        });
        */
      });
 
    function fun_view(id)
    {
      //alert(id);
      var view_url = server + '/api/empresas/'+id;
      $.ajax({
        url: view_url,
        type:"GET", 
        success: function(result){
            var str = result;
            console.log(result);
            console.log(result[0].nombre);
                        //alert(result.data.nombre);
                        //alert(result.data.nombre);
          //alert(id);
          $("#view_codigo").text(result[0].id);
          $("#view_nombre").text(result[0].nombre);
          $("#view_direccion").text(result[0].direccion);
          $("#view_web").text(result[0].web);
          $("#view_fecha_creacion").text(result[0].created_at);
          $("#view_updated").text(result.updated_at);
          

        },
        error: function (result) {
            alert(result);
          }
      });
    }
 
    function fun_edit(id)
    {
      var view_url = server + '/api/empresas/'+id;
      $.ajax({
        url: view_url,
        type:"GET", 
        success: function(result){
            var str = result;
            console.log(result);
            console.log(result[0].nombre);
                        //alert(result.data.nombre);
                        //alert(result.data.nombre);
          //alert(id);
          $("#edit_codigo").val(result[0].id);
          $("#edit_nombre").val(result[0].nombre);
          $("#edit_direccion").val(result[0].direccion);
          $("#edit_web").val(result[0].web);
         }
      });
    }

          function fun_inserta() {
        //alert("intento insertar");
        var view_url = server + '/api/empresas';
        var nombre = $("#nombre").val();
        var direccion = $("#direccion_").val();
        var web = $("#web").val();
        //alert(url + " " + nombre + " " + telefono + " " + correo + " " + observaciones  + " " + created_by + " " + token);
        //alert(id);
        alert(nombre+' '+direccion+' '+web);
        $.ajax({
          url: view_url,
          type: "POST",
          datatype: "json",
          data: {
            "nombre": nombre,
            "direccion": direccion,
            "web": web
            },
          success: function (response) {
            //alert(obj.nombre);
            alert(response.respuesta);
            console.log(response);
            location.reload();
            //alert(result.data.nombre);
          },
          error: function () {
            alert("error en peticion");
          }
        });
      }



 
  function funUpdate() {
        //alert("intento insertar");
        var id= $("#edit_codigo").val();
        var view_url = server + '/api/empresas/'+id;
        var direccion = $("#edit_direccion").val();
        var web = $("#edit_web").val();
        var nombre = $("#edit_nombre").val();
        //alert(codigo_banco + " " + telefono + " " + nombre + " " + observaciones + " " + web + " " + observaciones + " " + modified_by + " " + token);
        //alert(nombre);
        //alert(id);
        console.log(id+' '+direccion+' '+web+' '+nombre);
        $.ajax({
          url: view_url,
          type: "PUT",
          datatype: "json",   
          data: {
            "nombre": nombre,
            "direccion": direccion,
            "web": web
            
          },
          success: function (response) {
            //alert(obj.nombre);
            console.log(response);
            alert(response.respuesta);
            location.reload();
            //alert(result.data.nombre);
          },
          error: function (response) {
            alert(response);
          }
        });
      }

    function fun_delete(id)
    {
      var conf = confirm("Esta seguro que desea eliminar No."+id+"??");
      if(conf){
        var delete_url = server + '/api/empresas/'+id;
        $.ajax({
          url: delete_url,
          type:"DELETE",  
          success: function(response){
            alert(response.respuesta);
            location.reload(); 
          }
        });
      }
      else{
        return false;
      }
    }
 </script>       
          </div>
          <!-- /.col-lg-12 -->
      </div>
</div>

@endsection
