<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Gestion de documentos</title>


    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
    <!-- Styles --><!-- Styles -->
    <link rel="stylesheet" href="{{ url('/')}}/css/bootstrap.min.css">
    <!-- Styles -->
    <link rel="stylesheet" href="{{ url('/')}}/css/font-awesome.min.css">
     <link href="{{ url('/') }}/css/bootstrap.css" rel="stylesheet" type="text/css">
    <!-- Custom Fonts -->
    <link href="{{ url('/') }}/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Mis Estilos CSS-->
    <link href="{{ url('/') }}/css/estilos.css" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" type="image/x-icon" href="{{ url('/') }}/favicon.ico" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />   

    <link rel="stylesheet" href="{{ url('/')}}/css/w3.css">
    <script src="{{ url('/') }}/js/Chart.js"></script>
    <!-- Scripts -->
    
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    <script type="text/javascript">
        function myfocus() {
            console.log("Perro curioso");
            
            $(window).on('load',function(){
                $('#addModalDetalle').modal('show');
            });
            
            

            
            
            document.getElementById("nPedido").focus();
            document.getElementById("codigo").focus();
            document.getElementById("btnCerrarPedido").focus();
            


}
    </script>
</head>
<body>
    <div id="app">

    @if(Auth::user()->tipo_usuario=='1')
        <!-- administrador -->
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <strong>MIS GESTIONES <span class="fa fa-codepen"></span></strong>
                        
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ url('/login') }}">Login</a></li>
                            <li><a href="{{ url('/register') }}">Registro</a></li>
                        @else


                                    <li>                          
                                        <a href="{{ url('/empresas') }}"> <span class="fa fa-building"></span> Empresas</a>
                                    </li>
                                       <li>                          
                                        <a href="{{ url('/oficinas') }}"> <span class="fa fa-cubes"></span> Oficinas</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/estados') }}"> <span class="fa fa-exchange"></span> Estados</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/gestion_casos') }}" > <span class="fa fa-folder-open"></span> Gestion de casos </a>
                                    </li>
                                     <li>
                                        <a href="{{ url('/tipo_casos') }}" > <span class="fa fa-gears"></span> Tipo de Gestion de casos </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/usuario') }}" > <span class="fa fa-group"></span> Usuario</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/reportes') }}"> <span class="fa fa-bar-chart"></span> Reportes</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>

                         
                        @endif
                    </ul>
                </div>
            </div>
        </nav>


        <div class="w3-sidebar w3-bar-block w3-collapse w3-card w3-animate-left" style="width:200px;" id="mySidebar">
          <button class="w3-bar-item w3-button w3-large w3-hide-large" onclick="w3_close()">Close &times;</button>
          
            <a href="" title="Vea su información como usuario" class="w3-bar-item w3-button" data-toggle="modal" data-target="#viewModaluser" onclick="fun_view_user('{{ Auth::user()->id }}')"> <span class="   fa fa-codepen"></span> <strong> BIENVENDO ADMIN {{ Auth::user()->name }}</strong></a>
             <a href="{{ url('/empresas') }}" class="w3-bar-item w3-button"> <span class="fa fa-building"></span> Empresas</a>
             <a href="{{ url('/oficinas') }}" class="w3-bar-item w3-button"> <span class="fa fa-cube"></span> Mis Oficinas</a>
             <a href="{{ url('/estados') }}" class="w3-bar-item w3-button"> <span class="fa fa-exchange"></span> Estados</a>
             <a href="{{ url('/gestion_casos') }}" class="w3-bar-item w3-button"> <span class="fa fa-folder-open"></span> Gestion de casos </a>
             <a href="{{ url('/tipo_casos') }}" class="w3-bar-item w3-button"> <span class="fa fa-gears"></span> Tipo de Gestion de casos </a>
             <a href="{{ url('/usuario') }}" class="w3-bar-item w3-button"> <span class="fa fa-group"></span> Usuario</a>
             <a href="{{ url('/reportes') }}" class="w3-bar-item w3-button"> <span class="fa fa-bar-chart"></span> Reportes</a>
            <input type="hidden" name="hidden_view_user" id="hidden_view_user" value="{{url('usuarios/view')}}">
         

         
        </div>

         <!-- fin admin -->
    @elseif(Auth::user()->tipo_usuario=='2')
        <!-- ENCARGADO -->
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <strong>MIS GESTIONES <span class="fa fa-codepen"></span></strong>
                        
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ url('/login') }}">Login</a></li>
                            <li><a href="{{ url('/register') }}">Registro</a></li>
                        @else


                                    <li>                          
                                        <a href="{{ url('/empresas') }}"> <span class="fa fa-building"></span> Empresas</a>
                                    </li>
                                       <li>                          
                                        <a href="{{ url('/oficinas') }}"> <span class="fa fa-cubes"></span> Oficinas</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/estados') }}"> <span class="fa fa-exchange"></span> Estados</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/gestion_casos') }}" > <span class="fa fa-folder-open"></span> Gestion de casos </a>
                                    </li>
                                     <li>
                                        <a href="{{ url('/tipo_casos') }}" > <span class="fa fa-gears"></span> Tipo de Gestion de casos </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/usuario') }}" > <span class="fa fa-group"></span> Usuario</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/reportes') }}"> <span class="fa fa-bar-chart"></span> Reportes</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>

                         
                        @endif
                    </ul>
                </div>
            </div>
        </nav>


        <div class="w3-sidebar w3-bar-block w3-collapse w3-card w3-animate-left" style="width:200px;" id="mySidebar">
          <button class="w3-bar-item w3-button w3-large w3-hide-large" onclick="w3_close()">Close &times;</button>
          
            <a href="" title="Vea su información como usuario" class="w3-bar-item w3-button" data-toggle="modal" data-target="#viewModaluser" onclick="fun_view_user('{{ Auth::user()->id }}')"> <span class="   fa fa-codepen"></span> <strong> BIENVENDO ADMIN {{ Auth::user()->name }}</strong></a>
             <a href="{{ url('/empresas') }}" class="w3-bar-item w3-button"> <span class="fa fa-building"></span> Empresas</a>
             <a href="{{ url('/oficinas') }}" class="w3-bar-item w3-button"> <span class="fa fa-cube"></span> Mis Oficinas</a>
             <a href="{{ url('/estados') }}" class="w3-bar-item w3-button"> <span class="fa fa-exchange"></span> Estados</a>
             <a href="{{ url('/gestion_casos') }}" class="w3-bar-item w3-button"> <span class="fa fa-folder-open"></span> Gestion de casos </a>
             <a href="{{ url('/tipo_casos') }}" class="w3-bar-item w3-button"> <span class="fa fa-gears"></span> Tipo de Gestion de casos </a>
             <a href="{{ url('/usuario') }}" class="w3-bar-item w3-button"> <span class="fa fa-group"></span> Usuario</a>
             <a href="{{ url('/reportes') }}" class="w3-bar-item w3-button"> <span class="fa fa-bar-chart"></span> Reportes</a>
            <input type="hidden" name="hidden_view_user" id="hidden_view_user" value="{{url('usuarios/view')}}">
         

         
        </div>

         <!-- fin CLIENTE -->
    @elseif(Auth::user()->tipo_usuario=='cliente')
          <!-- CLIENTE -->
          <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <strong>MIS GESTIONES <span class="fa fa-codepen"></span></strong>
                        
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ url('/login') }}">Login</a></li>
                            <li><a href="{{ url('/register') }}">Registro</a></li>
                        @else


                                    <li>                          
                                        <a href="{{ url('/empresas') }}"> <span class="fa fa-building"></span> Empresas</a>
                                    </li>
                                       <li>                          
                                        <a href="{{ url('/oficinas') }}"> <span class="fa fa-cubes"></span> Oficinas</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/estados') }}"> <span class="fa fa-exchange"></span> Estados</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/gestion_casos') }}" > <span class="fa fa-folder-open"></span> Gestion de casos </a>
                                    </li>
                                     <li>
                                        <a href="{{ url('/tipo_casos') }}" > <span class="fa fa-gears"></span> Tipo de Gestion de casos </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/usuario') }}" > <span class="fa fa-group"></span> Usuario</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/reportes') }}"> <span class="fa fa-bar-chart"></span> Reportes</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>

                         
                        @endif
                    </ul>
                </div>
            </div>
        </nav>


        <div class="w3-sidebar w3-bar-block w3-collapse w3-card w3-animate-left" style="width:200px;" id="mySidebar">
          <button class="w3-bar-item w3-button w3-large w3-hide-large" onclick="w3_close()">Close &times;</button>
          
            <a href="" title="Vea su información como usuario" class="w3-bar-item w3-button" data-toggle="modal" data-target="#viewModaluser" onclick="fun_view_user('{{ Auth::user()->id }}')"> <span class="   fa fa-codepen"></span> <strong> BIENVENDO ADMIN {{ Auth::user()->name }}</strong></a>
             <a href="{{ url('/empresas') }}" class="w3-bar-item w3-button"> <span class="fa fa-building"></span> Empresas</a>
             <a href="{{ url('/oficinas') }}" class="w3-bar-item w3-button"> <span class="fa fa-cube"></span> Mis Oficinas</a>
             <a href="{{ url('/estados') }}" class="w3-bar-item w3-button"> <span class="fa fa-exchange"></span> Estados</a>
             <a href="{{ url('/gestion_casos') }}" class="w3-bar-item w3-button"> <span class="fa fa-folder-open"></span> Gestion de casos </a>
             <a href="{{ url('/tipo_casos') }}" class="w3-bar-item w3-button"> <span class="fa fa-gears"></span> Tipo de Gestion de casos </a>
             <a href="{{ url('/usuario') }}" class="w3-bar-item w3-button"> <span class="fa fa-group"></span> Usuario</a>
             <a href="{{ url('/reportes') }}" class="w3-bar-item w3-button"> <span class="fa fa-bar-chart"></span> Reportes</a>
            <input type="hidden" name="hidden_view_user" id="hidden_view_user" value="{{url('usuarios/view')}}">
         

         
        </div>

         <!-- fin ENCARGADO -->
    @endif


        

        

                @yield('content')
      
           
        </div>
        
    <script>
        function w3_open() {
            document.getElementById("mySidebar").style.display = "block";
        }
        function w3_close() {
            document.getElementById("mySidebar").style.display = "none";
        }

        function myFunctionPass()
        {
            //alert("hola");
            var cmp = document.getElementById("edit_pas").value;
            var cmppass = document.getElementById("edit_pass").value;
            if(cmp==cmppass)
            {
                alert("si coincide");
            }
            else
            {
             

            }
        }
            function fun_view_user(id)
            {
              var view_url = $("#hidden_view_user").val();
              $.ajax({
                url: view_url,
                type:"GET", 
                data: {"id":id}, 
                success: function(result){
                  console.log(result);
                  //alert(id);
                  $("#view_name_user").text(result.name);
                  $("#view_direccion_user").text(result.direccion);
                  $("#view_telefono_user").text(result.telefono);
                  $("#view_email_user").text(result.email);
                  $("#view_nit_user").text(result.nit);
                  $("#view_departamento_user").text(result.nombre_departamento);
                  $("#view_municipio_user").text(result.nombre_municipio);
                  $("#view_tipo_usuario_user").text(result.tipo_usuario);
                  $("#view_tipo_entrega_user").text(result.tipo_entrega);
                  $("#view_observaciones_user").text(result.observaciones);
                  

                }
              });
            }
         
        </script>
        
    </div>

    <!-- Scripts -->
    <script type="text/javascript" language="javascript" src="{{ url('/') }}/Datatables/media/js/jquery.dataTables.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.4.js">
    </script>
    <script type="text/javascript" language="javascript" class="init"></script>   
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
    <script src="{{ url('/') }}/js/Chart.bundle.min.js"></script>  
    <script src="{{ url('/') }}/js/Chart.min.js"></script>
    <script type="text/javascript" language="javascript" class="init"></script>
    <script src="{{ url('/') }}/js/myapp.js"></script>
    <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    
</body>
</html>
