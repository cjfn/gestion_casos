<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
 <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
    <!-- Styles --><!-- Styles -->
    <link rel="stylesheet" href="{{ url('/')}}/css/bootstrap.min.css">
    <!-- Styles -->
    <link rel="stylesheet" href="{{ url('/')}}/css/font-awesome.min.css">
     <link href="{{ url('/') }}/css/bootstrap.css" rel="stylesheet" type="text/css">
    <!-- Custom Fonts -->
    <link href="{{ url('/') }}/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Mis Estilos CSS-->
    <link href="{{ url('/') }}/css/estilos.css" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" type="image/x-icon" href="{{ url('/') }}/favicon.ico" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />   

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Administrame</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    <div id="app">
        
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
